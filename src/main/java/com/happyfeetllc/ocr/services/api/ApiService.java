package com.happyfeetllc.ocr.services.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.happyfeetllc.ocr.domain.enums.ResponseStatus;
import com.happyfeetllc.ocr.domain.models.FootSizeData;
import com.happyfeetllc.ocr.domain.models.StandardResponse;
import com.happyfeetllc.ocr.services.filestream.FileStreamService;
import com.happyfeetllc.ocr.services.scanner.ImageDetectorService;

import javax.servlet.MultipartConfigElement;

import static spark.Spark.*;

@Singleton
public class ApiService
{
    @Inject
    private FileStreamService fileStreamService;

    @Inject
    private ImageDetectorService imageDetectorService;

    public void run()
    {
        staticFiles.externalLocation("public");

        port(8080);
        setRoutes();
    }

    private void setRoutes()
    {
        get("/hello", (req, res) -> "Hello World");

        post("/ocr", (req, res) -> {
            // Prepare response buffer
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
            res.type("application/json");

            // Save uploaded image to memory
            String tempFile = fileStreamService.saveFileFromResponse(req.raw().getPart("uploaded_file").getInputStream());

            // Scan the uploaded image and retrieve data
            FootSizeData footSizeData = imageDetectorService.detectByPath(tempFile);

            // Initialize response message and reply to client
            StandardResponse response = new StandardResponse(ResponseStatus.SUCCESS, new Gson().toJsonTree(footSizeData));
            return new Gson().toJson(response);
        });
    }
}
