package com.happyfeetllc.ocr.services.filestream;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Singleton
public class FileStreamService {
    private final File uploadDir;

    @Inject
    public FileStreamService()
    {
        uploadDir = new File("upload");
        uploadDir.mkdir();
    }

    private Path getPath()
    {
        return uploadDir.toPath();
    }

    private Path createTempFile() throws IOException {
        return Files.createTempFile(getPath(), "", "");
    }

    public String saveFileFromResponse(InputStream input) throws IOException {
        Path tempFile = createTempFile();
        Files.copy(input, tempFile, StandardCopyOption.REPLACE_EXISTING);
        return tempFile.toAbsolutePath().toString();
    }
}
