package com.happyfeetllc.ocr.services;

import com.google.inject.AbstractModule;
import com.happyfeetllc.ocr.services.api.ApiService;
import com.happyfeetllc.ocr.services.filestream.FileStreamService;
import com.happyfeetllc.ocr.services.scanner.ImageDetectorService;
import com.happyfeetllc.ocr.services.scanner.PaperDetectorService;

public class ServicesModule extends AbstractModule {
    @Override
    protected void configure() {
        // Bind scanner services
        bind(ImageDetectorService.class).asEagerSingleton();
        bind(PaperDetectorService.class).asEagerSingleton();

        // Bind File Stream service
        bind(FileStreamService.class).asEagerSingleton();

        // Bind API Service
        bind(ApiService.class).asEagerSingleton();
    }
}
