package com.happyfeetllc.ocr.services.scanner;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.happyfeetllc.ocr.domain.models.FootSizeData;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

@Singleton
public class ImageDetectorService {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

    @Inject
    private PaperDetectorService paperDetectorService;

    private FootSizeData detect(Mat image) {
        Mat detectedImg = paperDetectorService.detect(image);
        List<MatOfPoint> paperContours = paperDetectorService.getPaperContours();
        Size fSize = new Size(paperDetectorService.getFootDimensions());

        Map.Entry<Mat, Size> detectedImage = new AbstractMap.SimpleEntry<>
                (detectedImg, fSize);
        if (detectedImg != null && paperContours.size()>0) {
            Size size = detectedImage.getValue();
            return new FootSizeData(size.width, size.height);
        }
        return null;
    }

    public FootSizeData detectByPath(String path)
    {
        Mat img = Imgcodecs.imread(path);
        return detect(img);
    }
}
