package com.happyfeetllc.ocr.services.scanner;

import com.google.inject.Singleton;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Singleton
public class PaperDetectorService {
    private List<MatOfPoint> paperContours = Collections.synchronizedList(new ArrayList<>());
    private int paperCounter;
    private double[] footDimensions = new double[2];

    List<MatOfPoint> getPaperContours() {
        if (paperCounter++ > 10) {
            paperContours.clear();
        }
        return paperContours;
    }

    double[] getFootDimensions() {
        double[] fDimens = new double[2];
        fDimens[0] = footDimensions[0];
        fDimens[1] = footDimensions[1];
        return fDimens;
    }

    private double ratioPaper(Point[] points) {
        double[] lengths = new double[4];
        lengths[0] = dist(points[0], points[points.length - 1]);
        for (int i = 0; i < points.length - 1; i++) {
            lengths[i + 1] = dist(points[i], points[i + 1]);
        }
        double parallelRatio = 0;
        for (int i = 0; i < lengths.length / 2; i++) {
            double ratio = Math.abs(1.0 * lengths[i] / lengths[i + 2]);
            if (ratio != 0 && ratio < 1) ratio = 1.0 / ratio;
            parallelRatio += ratio / 2;
        }
        double meanRatio = 0;
        for (int i = 0; i < lengths.length - 1; i++) {
            double ratio = Math.abs(1.0 * lengths[i] / lengths[i + 1]);
            if (ratio < 1 && ratio != 0) ratio = 1.0 / ratio;
            meanRatio += ratio / 3;
        }
        double meanError = meanRatio - 297.0 / 210;
        double meanParalelError = parallelRatio - 1;
        return 1.0 / (meanError * meanError + meanParalelError * meanParalelError);
    }

    private double parallelogramError(Point[] points) {
        Point[] midPoints = new Point[2];
        midPoints[0] = midPoint(points[0], points[2]);
        midPoints[1] = midPoint(points[1], points[3]);

        return dist(midPoints[0], midPoints[1]);
    }

    private Point midPoint(Point a, Point b) {
        return new Point((a.x + b.x) / 2, (a.y + b.y) / 2);
    }

    private void prepareThreshold(Mat image, int alpha) {
        Mat kernel = Mat.ones(3, 3, CvType.CV_8U);
        Point anchor = new Point(-1, -1);
        switch (alpha) {
            case 0:
                Imgproc.Canny(image, image, 15, 30, 3, false);
                Imgproc.dilate(image, image, kernel, anchor, 1);
                Core.bitwise_not(image, image);
                break;

            case 1:
                Imgproc.adaptiveThreshold(image, image, 255,
                        Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 5, alpha);
                Imgproc.erode(image, image, kernel, anchor, 1);
                break;

            case 2:
                /*Without blur*/
                Imgproc.Canny(image, image, 25, 85, 3, false);
                Imgproc.dilate(image, image, kernel, anchor, 1);
                Imgproc.morphologyEx(image, image, Imgproc.MORPH_CLOSE, kernel, anchor, 2);
                Core.bitwise_not(image, image);
                break;
        }
    }

    private double[] getWidthHeight(Point[] rect) {
        double widthA = dist(rect[0], rect[1]);
        double widthB = dist(rect[2], rect[3]);
        double width = Math.max(widthA, widthB);
        double heightA = dist(rect[1], rect[2]);
        double heightB = dist(rect[0], rect[3]);
        double height = Math.max(heightA, heightB);
        return new double[]{Math.min(width, height), Math.max(width, height)};
    }

    private void expandMiddlePoint(Point expanding,
                                   Point effecting, double distance) {
        double dx = effecting.x - expanding.x;
        double dy = effecting.y - expanding.y;
        double angle = Math.atan2(dy, dx);
        double x1 = Math.cos(angle);
        double y1 = Math.sin(angle);
        expanding.y += distance * y1;
        expanding.x += distance * x1;
    }

    private double dist(Point a, Point b) {
        double dx = a.x - b.x;
        double dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    private List<MatOfPoint2f> findBestContour(
            Mat image, List<MatOfPoint> contours) {
        return findBestContour(image, contours, 8);
    }

    private List<MatOfPoint2f> findBestContour(
            Mat image, List<MatOfPoint> contours, int expansion) {
        MatOfPoint2f approx = new MatOfPoint2f();
        int imgWidth, imgHeight;
        imgWidth = image.width();
        imgHeight = image.height();

        List<MatOfPoint2f> result = new ArrayList<>();

        MatOfPoint2f bestContour = null;//new MatOfPoint2f(contours.get(0).toArray());
        double bestRatio = Double.NEGATIVE_INFINITY;
        for (MatOfPoint pointContour : contours) {
            MatOfPoint2f contour = new MatOfPoint2f(pointContour.toArray());
            double perimeter = Imgproc.arcLength(contour, true);
            Imgproc.approxPolyDP(contour, approx, perimeter * 0.02, true);
            Point[] approxArray = approx.toArray();
            if (approxArray.length > 5 && approxArray.length < 15) {

                RotatedRect roundedRect = Imgproc.minAreaRect(contour);
                Point[] rectangle = new Point[4];
                roundedRect.points(rectangle);

                MatOfPoint2f minRect = new MatOfPoint2f(rectangle);
                if (minRect.empty()) continue;

                double rectArea = Imgproc.contourArea(minRect);

                Point[] rectPoints = minRect.toArray();
                Point[] newRect = new Point[4];
                boolean nearEdges = false;
                for (int r = 0; r < 4; r++) {
                    double minDistance = Double.POSITIVE_INFINITY;
                    Point nearestPoint = rectPoints[r];
                    for (Point point : approx.toArray()) {
                        double distance = dist(rectPoints[r], point);
                        if (distance < minDistance) {
                            minDistance = distance;
                            nearestPoint = point;
                        }
                    }
                    newRect[r] = nearestPoint;
                    if (nearestPoint.x < 10 || nearestPoint.y < 10
                            || nearestPoint.x > imgWidth - 10
                            || nearestPoint.y > imgHeight - 10) {
                        nearEdges = true;
                    }
                }
                Point[] oldRect = new Point[4];
                for (int r = 0; r < 4; r++) {
                    oldRect[r] = newRect[r].clone();
                }
                double rectPerimeter = Imgproc.arcLength(minRect, true);
                //Negative expansion is considered relative expansion
                if (expansion < 0) {
                    expansion = (int) (-expansion *
                            (rectPerimeter / (imgWidth + imgHeight)));
                }
                expandRectangle(newRect, oldRect, expansion);

                if (nearEdges) continue;
                double[] paperDimensions = getWidthHeight(newRect);
                double parallelError = parallelogramError(newRect);
                MatOfPoint2f rect = new MatOfPoint2f(newRect);
                rectPerimeter = Imgproc.arcLength(rect, true);
                if (parallelError > rectPerimeter / 40) continue;
                MatOfPoint approxPoints = new MatOfPoint(approxArray);
                rectArea = Imgproc.contourArea(rect);
                MatOfInt hull = new MatOfInt();
                Imgproc.convexHull(approxPoints, hull);
                MatOfInt4 defects = new MatOfInt4();
                //Imgproc.convexityDefects(rect, hull, defects);
                Imgproc.convexityDefects(approxPoints, hull, defects);
                if (defects.rows() > 3) continue;
                int biggestDepthIndex = 0;
                double biggestDepth = Double.NEGATIVE_INFINITY;
                for (int j = 0; j < defects.rows(); j++) {
                    double depth = defects.get(j, 0)[3];
                    if (biggestDepth < depth) {
                        biggestDepth = depth;
                        biggestDepthIndex = j;
                    }
                }
                double[] biggestDefect = defects.get(biggestDepthIndex, 0);
                Point[] defectPoint = new Point[3];
                if (biggestDepth / 256 < paperDimensions[1] / 3) continue;
                if (biggestDepth / 256 > paperDimensions[1] - 5) continue;
                for (int p = 0; p < 3; p++) {
                    defectPoint[p] = approxArray[(int) biggestDefect[p]];
                }
                if (bestContour == null) bestContour = rect;
                MatOfPoint2f defectHull = new MatOfPoint2f(defectPoint);
                double areaRatio = Imgproc.contourArea(defectHull) / rectArea;
                if (areaRatio > 0.2 && areaRatio < 0.7
                        && biggestDefect[3] / 256 > rectPerimeter / 7) {

                    //return rect;
                    double score = ratioPaper(newRect);
                    if (score > bestRatio) {
                        bestRatio = score;
                        result.clear();
                        footDimensions[1] = 297 * biggestDepth / 256 / paperDimensions[1];
                        bestContour = contour;
                        RotatedRect r = Imgproc.minAreaRect(rect);
                        Point[] re = new Point[4];
                        r.points(re);
                        result.add(contour);
                        result.add(rect);
                        result.add(defectHull);

                    }

                }

            }
        }
        return result;
    }

    private Mat fourPointTransform(Mat image, MatOfPoint rectangle) {
        int maxWidth = 210 * 2;
        int maxHeight = 297 * 2;
        Point[] destPoints = {new Point(0, 0), new Point(maxWidth - 1, 0),
                new Point(maxWidth - 1, maxHeight - 1), new Point(0, maxHeight - 1)};
        MatOfPoint2f dst = new MatOfPoint2f(destPoints);
        Point[] rectArray = rectangle.toArray();
        if (dist(rectArray[0], rectArray[1]) > dist(rectArray[1], rectArray[2])) {
            Point temp = rectArray[3];
            rectArray[3] = rectArray[2];
            rectArray[2] = rectArray[1];
            rectArray[1] = rectArray[0];
            rectArray[0] = temp;
        }
        MatOfPoint2f rect = new MatOfPoint2f(rectArray);
        Mat warpMatrix = Imgproc.getPerspectiveTransform(rect, dst);
        Mat warped = new Mat();
        Imgproc.warpPerspective(image, warped, warpMatrix,
                new Size(maxWidth, maxHeight));

        return warped;

    }

    private Mat findFootDimensions(Mat image, boolean flipped) {
        Mat gray = new Mat();
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.medianBlur(gray, gray, 7);
        prepareThreshold(gray, 2);
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(gray, contours, new Mat(),
                Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        MatOfPoint contourPoints = contours.get(maxAreaContour(contours));
        MatOfPoint2f paperContour = new MatOfPoint2f(contourPoints.toArray());
        MatOfPoint2f approx = new MatOfPoint2f();
        Imgproc.approxPolyDP(paperContour, approx, Imgproc.arcLength(paperContour, true) * 0.02, true);
        Point[] approxArray = approx.toArray();
        MatOfPoint approxPoints = new MatOfPoint(approxArray);
        MatOfInt hull = new MatOfInt();
        Imgproc.convexHull(approxPoints, hull);
        MatOfInt4 defects = new MatOfInt4();
        Imgproc.convexityDefects(approxPoints, hull, defects);
        int biggestDepthIndex = 0;
        double biggestDepth = Double.NEGATIVE_INFINITY;
        for (int j = 0; j < defects.rows(); j++) {
            double depth = defects.get(j, 0)[3];
            if (biggestDepth < depth) {
                biggestDepth = depth;
                biggestDepthIndex = j;
            }
        }
        double[] biggestDefect = defects.get(biggestDepthIndex, 0);
        if (biggestDefect == null) return image;
        Point start = approxArray[(int) biggestDefect[0]];
        Size size = gray.size();
        if (start.y < size.height / 2 && !flipped) {
            Core.flip(image, image, -1);
            return findFootDimensions(image, true);
        }
        int margin = 20;
        int cutoff = (int) (size.height - biggestDepth / 256 * 2 / 3);
        Rect upperHalfRect = new Rect(margin, margin, (int) size.width - margin * 2, cutoff);
        Mat upperImage = new Mat(image, upperHalfRect);
        Imgproc.cvtColor(upperImage, gray, Imgproc.COLOR_BGR2GRAY);
        double otsu = Imgproc.threshold(gray, new Mat(), 200, 255, Imgproc.THRESH_OTSU);
        Imgproc.threshold(gray, gray, otsu * 10 / 9, 255, Imgproc.THRESH_BINARY_INV);
        List<MatOfPoint> footContours = new ArrayList<>();
        Imgproc.findContours(gray, footContours, new Mat(),
                Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        MatOfPoint2f bigContour = new MatOfPoint2f(
                footContours.get(maxAreaContour(footContours)).toArray());
        Point[] footRectPoints = new Point[4];
        RotatedRect footR = Imgproc.minAreaRect(bigContour);
        footR.points(footRectPoints);

        for (Point footRectPoint : footRectPoints) {
            footRectPoint.x += margin;
            footRectPoint.y += margin;
        }
        double minY = Double.POSITIVE_INFINITY;
        Point highestPoint = footRectPoints[0];
        double maxDx = 0;

        for (int i = 0; i < footRectPoints.length; i++) {
            Point currentPoint = footRectPoints[i];
            if (footRectPoints[i].y < minY) {
                minY = footRectPoints[i].y;
                highestPoint = footRectPoints[i];
            }

            Point nextPoint = i < 3 ? footRectPoints[i + 1] : footRectPoints[0];
            double dx = Math.abs(currentPoint.x - nextPoint.x);
            if (dx > maxDx) {
                maxDx = dx;
            }
        }

        int whileC = 0;
        while (!((footRectPoints[3].y > footRectPoints[2].y) &&
                (footRectPoints[3].y > footRectPoints[1].y) &&
                (footRectPoints[0].y > footRectPoints[2].y) &&
                (footRectPoints[0].y > footRectPoints[1].y) &&
                (footRectPoints[0].x < footRectPoints[2].x) &&
                (footRectPoints[0].x < footRectPoints[3].x) &&
                (footRectPoints[1].x < footRectPoints[2].x) &&
                (footRectPoints[1].x < footRectPoints[3].x))) {
            if (++whileC > 4) break;
            Point temp = footRectPoints[3];
            footRectPoints[3] = footRectPoints[2];
            footRectPoints[2] = footRectPoints[1];
            footRectPoints[1] = footRectPoints[0];
            footRectPoints[0] = temp;
        }
        footRectPoints[0].y = size.height;
        footRectPoints[3].y = size.height;
        footDimensions[0] = maxDx / 2;
        footDimensions[1] = (int) (297 - highestPoint.y / 2);
        MatOfPoint footRect = new MatOfPoint(footRectPoints);
        footContours.clear();
        footContours.add(footRect);
        if (image.channels() == 4) {
            Imgproc.drawContours(image, footContours, 0, new Scalar(0, 255, 0, 255), 2);
        } else {
            Imgproc.drawContours(image, footContours, 0, new Scalar(0, 255, 0), 2);
        }

        return image;
    }

    private int maxAreaContour(List<MatOfPoint> contours) {
        double area = 0;
        int c = 0;
        int index = -1;
        for (Iterator<MatOfPoint> iter = contours.iterator(); iter.hasNext(); c++) {
            MatOfPoint contour = iter.next();
            double contourArea = Imgproc.contourArea(contour);
            if (contourArea > area) {
                area = contourArea;
                index = c;
            }
        }
        return index;
    }

    Mat detect(Mat image) {
        Size size = image.size();
        Mat gray = new Mat(size, CvType.CV_8U);

        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);

        double resizeRatio = 1;
        if (Math.max(size.width, size.height) > 1200) {

            double ratio = size.width / size.height;

            if (size.height > size.width) {
                double height = 1200;
                resizeRatio = size.height / height;
                Imgproc.resize(gray, gray, new Size(height * ratio, height));
            } else {
                double width = 1200;
                resizeRatio = size.width / width;
                Imgproc.resize(gray, gray, new Size(width, width / ratio));
            }
        }
        Mat gray0 = gray.clone();
        Mat[] grays = new Mat[3];
        grays[2] = gray0.clone();
        Imgproc.GaussianBlur(gray0, gray0, new Size(5, 5), 0);
        for (int i = 0; i < 2; i++) {
            grays[i] = gray0.clone();
        }


        List<MatOfPoint> contours = new ArrayList<>();
        double imageArea = gray.size().area();
        for (int k = 0; k < 3; k++) {
            prepareThreshold(grays[k], k);

            List<MatOfPoint> newContours = new ArrayList<>();
            Imgproc.findContours(grays[k], newContours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
            newContours.removeIf(contour -> Imgproc.contourArea(contour) < imageArea / 40);
            contours.addAll(newContours);
            System.out.println("Method " + k + ": " + newContours.size());
        }

        List<MatOfPoint2f> bestContour = findBestContour(gray, contours);
        if (bestContour.size() > 0) {
            paperContours.clear();
            for (MatOfPoint2f contour2f : bestContour) {
                Point[] contourArray = contour2f.toArray();
                for (Point point : contourArray) {
                    point.x = point.x * resizeRatio;
                    point.y = point.y * resizeRatio;
                }
                paperContours.add(new MatOfPoint(contourArray));

            }
            paperCounter = 0;
            MatOfPoint rectangle = paperContours.get(1);

            Mat warped = fourPointTransform(image, rectangle);
            warped = removeMargins(warped, 30);
            return findFootDimensions(warped, false);


        }
//        return null;
        return image;
    }

    private void expandRectangle(Point[] newRect, Point[] rectangle, int expansion) {
        expandMiddlePoint(newRect[0], rectangle[1], -expansion);
        expandMiddlePoint(newRect[0], rectangle[3], -expansion);
        expandMiddlePoint(newRect[3], rectangle[0], -expansion);
        expandMiddlePoint(newRect[3], rectangle[2], -expansion);
        for (int r = 1; r < 3; r++) {
            expandMiddlePoint(newRect[r], rectangle[r + 1], -expansion);
            expandMiddlePoint(newRect[r], rectangle[r - 1], -expansion);
        }
    }

    private Mat removeMargins(Mat image, int margin) {
        int width = image.width();
        int height = image.height();
        Mat gray = new Mat();
        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        Mat upperLeft = new Mat(gray, new Rect(0, 0, margin, margin));
        Mat upperRight = new Mat(gray, new Rect(width - margin, 0, margin, margin));
        Mat lowerLeft = new Mat(gray, new Rect(0, height - margin, margin, margin));
        Mat lowerRight = new Mat(gray, new Rect(width - margin, height - margin, margin, margin));
        final int thresholdType = Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU;
        Imgproc.threshold(upperLeft, upperLeft, 210, 255, thresholdType);
        Imgproc.threshold(upperRight, upperRight, 210, 255, thresholdType);
        Imgproc.threshold(lowerLeft, lowerLeft, 210, 255, thresholdType);
        Imgproc.threshold(lowerRight, lowerRight, 210, 255, thresholdType);
        Core.rotate(upperRight, upperRight, Core.ROTATE_90_COUNTERCLOCKWISE);
        Core.rotate(lowerRight, lowerRight, Core.ROTATE_180);
        Core.rotate(lowerLeft, lowerLeft, Core.ROTATE_90_CLOCKWISE);
        Point upperLeftPoint = findWhitePoint(upperLeft);
        Point upperRightPoint = findWhitePoint(upperRight);
        Point lowerLeftPoint = findWhitePoint(lowerLeft);
        Point lowerRightPoint = findWhitePoint(lowerRight);
        Imgproc.drawMarker(upperLeft, upperLeftPoint, new Scalar(255, 0, 0));
        Imgproc.drawMarker(upperRight, upperRightPoint, new Scalar(255, 0, 0));
        Imgproc.drawMarker(lowerLeft, lowerLeftPoint, new Scalar(255, 0, 0));
        Imgproc.drawMarker(lowerRight, lowerRightPoint, new Scalar(255, 0, 0));
        int current = 0;
        org.opencv.imgcodecs.Imgcodecs.imwrite("C:\\imgs\\lowerLeft" + current + ".jpg", lowerLeft);
        org.opencv.imgcodecs.Imgcodecs.imwrite("C:\\imgs\\lowerRight" + current + ".jpg", lowerRight);
        org.opencv.imgcodecs.Imgcodecs.imwrite("C:\\imgs\\upperLeft" + current + ".jpg", upperLeft);
        org.opencv.imgcodecs.Imgcodecs.imwrite("C:\\imgs\\upperRight" + current + ".jpg", upperRight);
        upperRightPoint.x = margin - upperRightPoint.x;
        lowerRightPoint.x = margin - lowerRightPoint.x;
        lowerRightPoint.y = margin - lowerRightPoint.y;
        lowerLeftPoint.y = margin - lowerLeftPoint.y;
        upperRightPoint.x += width - margin;
        lowerLeftPoint.y += height - margin;
        lowerRightPoint.x += width - margin;
        lowerRightPoint.y += height - margin;
        Point[] points = {upperLeftPoint, upperRightPoint, lowerRightPoint, lowerLeftPoint};
        MatOfPoint newRect = new MatOfPoint(points);
        return fourPointTransform(image, newRect);
    }

    private Point findWhitePoint(Mat image) {
        return findUpperRightMostPoint(image);
    }

    private Point findUpperRightMostPoint(Mat image) {
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(image, contours, new Mat(),
                Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        double maxArea = 0;
        MatOfPoint contour = null;
        for (MatOfPoint c : contours) {
            Point[] points = c.toArray();
            for (Point p : points) {
                if (p.x + p.y > maxArea) {
                    maxArea = p.x + p.y;
                    contour = c;
                }
            }
        }
        Point rightmost = null;
        for (Point p : contour.toArray()) {
            if (p.x + p.y < maxArea || rightmost == null) {
                maxArea = p.x + p.y;
                rightmost = p;
            }
        }
        return rightmost;
    }
}
