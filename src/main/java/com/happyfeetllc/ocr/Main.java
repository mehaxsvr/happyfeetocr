package com.happyfeetllc.ocr;

import com.google.inject.Guice;
import com.happyfeetllc.ocr.services.ServicesModule;
import com.happyfeetllc.ocr.services.api.ApiService;

public class Main {

    public static void main(String[] args)
    {
        Guice.createInjector(new ServicesModule())
                .getInstance(ApiService.class)
                .run();
    }
}
