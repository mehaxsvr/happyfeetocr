package com.happyfeetllc.ocr.domain.models;

import com.google.gson.JsonElement;
import com.happyfeetllc.ocr.domain.enums.ResponseStatus;

public class StandardResponse {
    private ResponseStatus status;

    private JsonElement data;

    public StandardResponse(ResponseStatus status, JsonElement data)
    {
        this.status = status;
        this.data = data;
    }

    public ResponseStatus getStatus()
    {
        return status;
    }

    public JsonElement getData()
    {
        return data;
    }
}
