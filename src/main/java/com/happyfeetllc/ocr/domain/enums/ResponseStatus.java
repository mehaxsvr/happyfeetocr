package com.happyfeetllc.ocr.domain.enums;

public enum ResponseStatus {
    SUCCESS ("SUCCESS"),
    ERROR ("ERROR");

    private String status;

    ResponseStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
